#include "file_reader.h"
#include "constants.h"

#include <fstream>
#include <cstring>

void read(const char* file_name, curr_rate* array[], int& size)
{
    std::ifstream file(file_name);
    if (file.is_open())
    {
        size = 0;
        char tmp_buffer[MAX_STRING_SIZE];
        while (!file.eof())
        {
            curr_rate* rate = new curr_rate;
            file >> rate->bank;
            file >> rate->buy;
            file >> rate->sell;
            file.read(tmp_buffer, 1); // ������ ������� ������� �������
            file.getline(rate->adres, MAX_STRING_SIZE);
            array[size++] = rate;
        }
        file.close();
    }
    else
    {
        throw "������ �������� �����";
    }
}