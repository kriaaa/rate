#ifndef BOOK_SUBSCRIPTION_H
#define BOOK_SUBSCRIPTION_H

#include "constants.h"

struct curr_rate
{
    char bank[MAX_STRING_SIZE];
    double buy;
    double sell;
    char adres[MAX_STRING_SIZE];
};

#endif
