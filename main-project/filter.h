#pragma once
#ifndef FILTER_H
#define FILTER_H

#include "rate.h"

curr_rate** filter(curr_rate* array[], int size, bool (*check)(curr_rate* element), int& result_size);

bool check_by_bank(curr_rate* element);

bool check_by_sell(curr_rate* element);

#endif
