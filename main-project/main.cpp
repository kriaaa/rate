#include <iostream>

using namespace std;
#include "rate.h"
#include "file_reader.h"
#include "constants.h"
#include "filter.h"
void output(curr_rate* rate)
{
	cout << "******������******" << endl;
	cout << "����: " << rate->bank << '\n';
	cout << "����� � �������: " << rate->buy << " / " << rate->sell << '\n';
	cout << "�����:" << rate->adres << '\n';
	cout << '\n';
}

int main()
{
	setlocale(LC_ALL, "ru");
	cout << "Laboratory work #8. GIT\n";
	cout << "Variant #4. Currancy rates\n";
	cout << "Author: Darya Raketskaya\n";
	cout << "Group: 12\n";
	curr_rate* rate[MAX_FILE_ROWS_COUNT];
	int size;
	try
	{
		read("data.txt", rate, size);
		cout << "*****   *****\n\n";
		for (int i = 0; i < size; i++)
		{
			output(rate[i]);
		}
		bool (*check_function)(curr_rate*) = NULL;
		int item;
		cin >> item;
		cout << '\n';
		switch (item)
		{
		case 1:
			check_function = check_by_bank; //       
			cout << "*****�����������*****\n\n";
			break;
		case 2:
			check_function = check_by_sell; //       
			cout << "*****������� < 2.5*****\n\n";
			break;
		default:
			throw "  ";
		}
		if (check_function)
		{
			int new_size;
			curr_rate** filtered = filter(rate, size, check_function, new_size);
			for (int i = 0; i < new_size; i++)
			{
				output(filtered[i]);
			}
			delete[] filtered;
		}
		for (int i = 0; i < size; i++)
		{
			delete rate[i];
		}
	}
	catch (const char* error)
	{
		cout << error << '\n';
	}
	return 0;
}
