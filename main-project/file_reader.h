#ifndef READER_H
#define READER_H

#include "rate.h"

void read(const char* file_name, curr_rate* array[], int& size);

#endif